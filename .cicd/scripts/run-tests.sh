#!/usr/bin/env bash

COV_FAIL_UNDER=75
PY_VERSION='python3'

${PY_VERSION} -m coverage run --branch manage.py test && \
${PY_VERSION} -m coverage report -m && \
${PY_VERSION} -m coverage html -d report/ && \
rm -fR ../report/ && \
mv -f report/ ../ && \
${PY_VERSION} -m coverage xml --fail-under=$COV_FAIL_UNDER
