from typing import cast
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    FormView,
    ListView,
    UpdateView,
)

from forum.common.forms import (
    CategoryCreateForm,
    RegisterForm,
    PostForm,
    TopicForm,
    BlockTopicForm,
)
from forum.common.mixin import SuperUserMixin
from forum.common.models import (
    Category,
    Post,
    Topic,
)


class IndexView(ListView):
    model = Category


index_view = IndexView.as_view()


class CreateCategoryView(
    LoginRequiredMixin,
    SuperUserMixin,
    SuccessMessageMixin,
    CreateView
):
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('common:index')
    success_message = _('Category successfully created')


create_category_view = CreateCategoryView.as_view()


class DeleteCategoryView(
    LoginRequiredMixin,
    SuperUserMixin,
    SuccessMessageMixin,
    DeleteView
):
    model = Category
    success_url = reverse_lazy('common:index')
    success_message = _('Category successfully deleted')


delete_category_view = DeleteCategoryView.as_view()


class UpdateCategoryView(
    LoginRequiredMixin,
    SuperUserMixin,
    SuccessMessageMixin,
    UpdateView
):
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('common:index')
    template_name_suffix = '_update_form'
    success_message = _('Category successfully updated')


update_category_view = UpdateCategoryView.as_view()


class RegisterView(SuccessMessageMixin, CreateView):
    """ Create new user """
    form_class = RegisterForm
    success_url = reverse_lazy('common:index')
    template_name = 'registration/register.html'
    success_message = _('User successfully created')


register_view = RegisterView.as_view()


class TopicListView(ListView):
    """ List of topics from specified categories """
    model = Topic
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset """
        category_pk = self.kwargs['pk']
        return Topic.objects.filter(category_id=category_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Extend get context data """
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['category'] = Category.objects.get(id=self.kwargs['pk'])
        return context


topic_list_view = TopicListView.as_view()


class TopicCreateView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    """ Create new topic """
    model = Topic
    model2 = Post
    form_class = TopicForm
    form_class2 = PostForm
    template_name = 'common/topic_form.html'
    success_message = _('Topic created with success')
    success_url = reverse_lazy('common:index')

    def get_context_data(self, **kwargs):
        """ Extend default context """
        context = super().get_context_data(**kwargs)
        context['form2'] = self.form_class2()
        return context

    def post(self, request, *args, **kwargs):
        """ Extend post method """
        super().post(request, *args, **kwargs)
        form = self.form_class({'name': request.POST['name']})
        form2 = self.form_class2({'text': request.POST['text']})

        if form.is_valid() and form2.is_valid():
            with transaction.atomic():
                new_topic = self.model(
                    name=request.POST['name'],
                    category_id=self.kwargs['pk']
                )
                new_topic.save()
                first_post = self.model2(
                    text=request.POST['text'],
                    topic=new_topic,
                    author=request.user
                )
                first_post.save()
            return redirect(self.success_url)

        return render(request, self.template_name, {'form': form, 'form2': form2})


topic_create_view = TopicCreateView.as_view()


class TopicPostsView(ListView):
    """ List of posts from topic """
    model = Post
    form_class = PostForm
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset """
        topic_pk = self.kwargs['pk']
        return Post.objects.filter(topic_id=topic_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Extend get context data """
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['topic'] = Topic.objects.get(id=self.kwargs['pk'])
        context['form'] = self.form_class()
        return context


topic_posts_view = TopicPostsView.as_view()


class CreatePostView(LoginRequiredMixin, CreateView):
    """ Create new post by authenticated user """
    model = Post
    form_class = PostForm
    success_message = _('Post created with success')

    def get(self, *args, **kwargs):
        """ Override method """
        return redirect(
            reverse('common:topic_posts', kwargs=kwargs)
        )

    def post(self, request, *args, **kwargs):
        """ Override method """
        form = self.form_class({'text': request.POST['text']})
        if not form.is_valid():
            messages.error(request, _('Incorrect data in post form!'))
            return redirect(
                reverse('common:topic_posts', kwargs=kwargs)
            )
        post = self.model(
            text=request.POST['text'],
            topic_id=kwargs['pk'],
            author=request.user
        )
        post.save()
        messages.success(request, self.success_message)
        return redirect(
            reverse('common:topic_posts', kwargs=kwargs)
        )


create_post_view = CreatePostView.as_view()


class PostDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Post
    success_message = _('Post deleted')

    def get_success_url(self) -> str:
        listed_object = self.get_object()
        return cast(str, reverse_lazy("common:topic_posts", kwargs={'pk': listed_object.topic.id}))

    def post(self, request, *args: str, **kwargs):
        post = self.get_object()
        if (request.user.id == post.author_id) or request.user.is_superuser:
            messages.success(request, self.get_success_message(cleaned_data={}))
            return super().post(request, *args, **kwargs)
        messages.error(request, _("Removing post not allowed!"))
        return redirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        post = self.get_object()
        if (request.user.id == post.author_id) or request.user.is_superuser:
            messages.success(request, self.get_success_message(cleaned_data={}))
            return super().get(request, *args, **kwargs)
        messages.error(request, _('Removing post not allowed!'))
        return redirect(self.get_success_url())


post_delete_view = PostDeleteView.as_view()


class UpdateTopicView(
    LoginRequiredMixin,
    SuperUserMixin,
    SuccessMessageMixin,
    UpdateView
):
    model = Topic
    form_class = TopicForm
    success_url = reverse_lazy('common:index')
    template_name_suffix = '_update_form'
    success_message = _('Topic successfully updated')


update_topic_view = UpdateTopicView.as_view()


class BlockTopicView(LoginRequiredMixin,
                     SuperUserMixin,
                     SuccessMessageMixin,
                     UpdateView):
    model = Topic
    form_class = BlockTopicForm
    template_name_suffix = '_block_form'
    custom_message = "Blocked"
    success_message = "Topic status active: %(active)s"

    def get_success_url(self) -> str:
        listed_object = self.get_object()
        return cast(str, reverse_lazy("common:topics", kwargs={'pk': listed_object.category.id}))

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
            active=self.object.active,
        )


block_topic_view = BlockTopicView.as_view()
