image: python:3.9-slim-buster

variables:
  TF_PLAN: app.tfplan
  TF_STATE: app.tfstate

before_script:
  - apt-get update
  - apt-get upgrade -y
  - apt-get install -y make gettext
  - pip install --upgrade pip
  - pip install virtualenv
  - python3 -m venv venv
  - source venv/bin/activate
  - pip install -r forum/requirements.txt

stages:
  - test
  - prebuild
  - build
  - deploy
  - destroy

.terraform-job:
  image:
    name: hashicorp/terraform:1.0.11
    entrypoint: [""]
  before_script:
    - cd terraform/
    - terraform --version
    - terraform init
    - terraform providers
  only:
    - main


test-job:
  services:
    - postgres:12.2-alpine
  variables:
    POSTGRES_DB: forumdb
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_URL: postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres:5432/$POSTGRES_DB
  stage: test
  script:
    - make tests


static-analysis:
  stage: test
  script:
    - make run-static-analysis

compile-messages:
  stage: prebuild
  script:
    - make compilemessages
  artifacts:
    paths:
      - forum/locale/*/LC_MESSAGES/django.mo

terraform-plan-job:
  extends: .terraform-job
  stage: build
  script:
    - terraform plan -state=$TF_STATE -out=$TF_PLAN
  dependencies:
    - compile-messages
  artifacts:
    paths:
      - terraform/${TF_STATE}
      - terraform/${TF_PLAN}
      - forum/locale/*/LC_MESSAGES/django.mo
    reports:
      terraform: terraform/${TF_PLAN}
    expire_in: 10 days

terraform-apply-job:
  extends: .terraform-job
  stage: deploy
  script:
    - terraform apply -auto-approve -state=${TF_STATE} ${TF_PLAN}
  after_script:
    - terraform show
  dependencies:
    - terraform-plan-job
  artifacts:
    paths:
      - terraform/${TF_STATE}
      - terraform/${TF_PLAN}
    reports:
      terraform: terraform/${TF_PLAN}

terraform-destroy-job:
  extends: .terraform-job
  dependencies:
    - terraform-apply-job
  script:
    - terraform destroy -state=${TF_STATE} -auto-approve
  stage: destroy
  when: manual
